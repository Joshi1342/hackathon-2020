import mysql.connector, os
import tkinter as tk
from tkinter import *

# ----------------------- Global Variables ----------------------- #

CurrentDIR = os.path.dirname(__file__)
ImageDIR = CurrentDIR + r"\Images"


# ----------------------- Functions ----------------------- #
#
# The button opens the menu and destroy the welcome screen

def main():

    try:
        conn()
        print("Database connection can be established")
    except:
        print("Could not connect to the database!")
        sys.exit()

    root = tk.Tk()
    root.title("Welcome")
    root.geometry('1920x1080')
    root.attributes('-fullscreen', True)

    Image = tk.PhotoImage(file = ImageDIR + r"\test.png")
    photo = tk.PhotoImage(file = ImageDIR + r"\start.png")

    Start_Image = photo.subsample(
        12, 
        12
        )

    canvas = Canvas(
        root, 
        width = 1920, 
        height = 1080
        )

    canvas.pack()

    background_label = tk.Label(root, image = Image)
    canvas.create_image(
        0, 
        0, 
        anchor = NW, 
        image = Image)

    Start_Button = tk.Button(
        root, 
        image = Start_Image, 
        command = lambda:[root.destroy(), start()], 
        anchor = 'w',
        activebackground = '#000000', 
        bg = '#000000', 
        fg = '#B0C4DE', 
        borderwidth = 0
        )

    Start_Button_window = canvas.create_window(
        150, 
        290, 
        anchor = "nw", 
        window = Start_Button
        )
    
    root.mainloop()

# Function to Establish Connection to the Database

def conn():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        database="test"
    )

# Function to start the menu

def start():

    Menu = str(r'python {}\Query_Menu.py').format(CurrentDIR)

    os.system(Menu)


if __name__ == "__main__":
    main()