import mysql.connector, os
import tkinter as tk
from tkinter import *
from tkinter import simpledialog

# ----------------------- Global Variables ----------------------- #

CurrentDIR = os.path.dirname(__file__)
ImageDIR = CurrentDIR + r"\Images"


# ----------------------- Functions ----------------------- #
#
# Every Button leads to the destruction of the menu and starts the clicked Tool.

def main():

    # mydb = mysql.connector.connect(
    #     host="localhost",
    #     user="root",
    #     database="corona"
    # )

    root = tk.Tk()
    root.title('Query Menu')
    root.geometry('1920x1080')
    root.attributes('-fullscreen', True)
    
    Image = tk.PhotoImage(file = ImageDIR + r"\menu.png")

    Menu_Image = Image.subsample(
        2,
        2
    )

    canvas = Canvas(
        root,
        width = 1920,
        height = 1080
    )

    canvas.pack()

    canvas.create_image(
        0,
        0,
        anchor = NW,
        image = Menu_Image
    )

    Query = tk.Button(
        root,
        text = "  SQL Query   ",
        command = lambda:[root.destroy(), sql_query()],
        anchor = 'w',
        borderwidth = 0,
        activeforeground = '#777788',
        bg = '#444F8D'
    )

    Query_window = canvas.create_window(
        120,
        290,
        anchor = 'nw',
        window = Query
    )

    SQLCommand = tk.Button(
        root,
        text = 'SQL Command',
        command = lambda:[root.destroy(), sql_insert()],
        anchor = 'w',
        borderwidth = 0,
        activeforeground = '#777788',
        bg = '#444F8D'
    )

    SQLCommand_window = canvas.create_window(
        120,
        320,
        anchor = 'nw',
        window = SQLCommand
    )

    Assignment = tk.Button(
        root,
        text = 'Assignments',
        command = lambda:[root.destroy(), assignments()],
        anchor = 'w',
        borderwidth = 0,
        activeforeground = '#777788',
        bg = '#444F8D'
    )

    Assignment_window = canvas.create_window(
        120,
        350,
        anchor = 'nw',
        window = Assignment
    )

    Exit = tk.Button(
        root,
        text = 'Exit Application',
        command = lambda:[root.destroy()],
        anchor = 'w',
        borderwidth = 0,
        activeforeground = '#777788',
        bg = '#444F8D'
    )

    Exit_window = canvas.create_window(
        120,
        380,
        anchor = 'nw',
        window = Exit
    )

    root.mainloop()

# Function to enter the SQL Query Tool

def sql_query():

    SQLQuery = str(r'python {}\sqlquery.py').format(CurrentDIR)
    
    os.system(SQLQuery)

# Function to enter the SQL Insert Tool

def sql_insert():

    SQLInsert = str(r'python {}\sqlinsert.py').format(CurrentDIR)

    os.system(SQLInsert)

# Function to enter the add assignment Tool

def assignments():

    assignments_list = str(r'python {}\assignments.py').format(CurrentDIR)

    os.system(assignments_list)


# ----------------------- Main Method ----------------------- #

if __name__ == '__main__':
    main()