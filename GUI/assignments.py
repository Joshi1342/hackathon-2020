import mysql.connector, os
import tkinter as tk
from tkinter import *
from tkinter import Text

# ----------------------- Global Variables ----------------------- #

CurrentDIR = os.path.dirname(__file__)
ImageDIR = CurrentDIR + r"\Images"


# ----------------------- Functions ----------------------- #
#
# The "Exit to menu" button will destroy the current window and start the menu window.
# The "Save to Database" will start the save() function.

def main():

    global root

    root = tk.Tk()
    root.title('Assignments')
    root.geometry('390x205')
    root.grid_rowconfigure(5, weight = 1)

    # Global Variable giving feedback whether the Insert was successful or not

    global Status

    Status = ''

    # User Inputs

    global entry_quantity

    entry_quantity = Entry(
        root,
        width = 40
    )

    global entry_arrival

    entry_arrival = Entry(
        root,
        width = 40
    )

    global entry_recipientID

    entry_recipientID = Entry(
        root,
        width = 40
    )

    Storages = [
        'Berlin',
        'Bremen',
        'Köln',
        'Mannheim',
        'München'
    ]

    # Dropdown menu storage location

    global Default

    Default = StringVar(root)
    Default.set(Storages[0])

    dropdown = OptionMenu(
        root,
        Default,
        *Storages,
    )

    dropdown.config(bg = 'WHITE')

    global entry_transporterID

    entry_transporterID = Entry(
        root,
        width = 40
    )
    
    # Text labels + Integrate them into the root Window with grid

    assignment_quantity = tk.Label(
        root,
        text = 'Assignment_quantity:'
    )

    assignment_quantity.grid(row = 0)

    assignment_arrival = tk.Label(
        root,
        text = 'Assignment arrival:'
    )

    assignment_arrival.grid(row = 1)

    recipient_id = tk.Label(
        root,
        text = 'Recipient ID:'
    )

    recipient_id.grid(
        row = 2,
        column = 0
    )

    storage_location = tk.Label(
        root,
        text = 'Storage location:'
    )

    storage_location.grid(
        row = 3,
        column = 0
    )

    transporter_id = tk.Label(
        root,
        text = 'Transporter ID:'
    )

    transporter_id.grid(
        row = 4,
        column = 0
    )

    Status_Label = tk.Label(
        root,
        text = 'Status of Insert: ',
        relief = GROOVE
    )

    Status_Label.grid(
        row = 6,
        column = 0
    )

    Status = tk.Label(
        root,
        text = Status
    )

    Status.grid(
        row = 6,
        column = 1
    )

    # Buttons

    Exit = tk.Button(
        root,
        text = 'Exit to menu',
        command = lambda:[exit()],
        borderwidth = 1
    )

    Exit.grid(
        row = 5,
        padx = 1
    )

    Save = tk.Button(
        root,
        text = 'Save to Database',
        command = lambda:[save(entry_quantity.get(), entry_arrival.get(), entry_recipientID.get(), Default.get(), entry_transporterID.get())],
        borderwidth = 1
    )

    Save.grid(
        row = 5,
        column = 1
    )

    # Integrate User Input fields into the root window with grid

    entry_quantity.grid(
        row = 0,
        column = 1
    )

    entry_arrival.grid(
        row = 1,
        column = 1
    )

    entry_recipientID.grid(
        row = 2,
        column = 1
    )

    dropdown.grid(
        row = 3,
        column = 1,
        sticky = 'ew'
    )

    entry_transporterID.grid(
        row = 4,
        column = 1
    )

    root.mainloop()

# Function to Establish Connection to the Database

def conn():
    global mydb
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        database="corona"
    )

# Function to Insert User Input into Database

def save(en_quantity, en_arrival, en_recipentID, en_storageLocation, en_transporterID):

    sql = str("""INSERT INTO Assignment(
        Assignment_quantity,
        Assignment_arrival,
        Recipent_ID,
        Storage_location,
        Transporter_ID
    )

    VALUES (
        '{}',
        '{}',
        '{}',
        '{}',
        '{}'
    );""".format(
        en_quantity, 
        en_arrival, 
        en_recipentID,
        en_storageLocation, 
        en_transporterID)
    )

    print(sql)
    
    entry_arrival.delete(0, 'end')
    entry_quantity.delete(0, 'end')
    entry_recipientID.delete(0, 'end')
    entry_transporterID.delete(0, 'end')

    try:
        conn()

        cursor = mydb.cursor()
        cursor.execute(sql)
        mydb.commit()

        Status.config(text = 'Successfully Inserted Assignment into Database')

    except:
        Status.config(text = 'Failed to Insert Assignment into Database :(')

# Function to return back to the Main menu.

def exit():

    root.destroy()

    Menu = str(r'python {}\Query_Menu.py').format(CurrentDIR)

    os.system(Menu)


# ----------------------- Main Method ----------------------- #

if __name__ == '__main__':
    main()