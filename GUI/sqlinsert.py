import mysql.connector, os
import tkinter as tk
from tkinter import *
from tkinter import Text

# ----------------------- Global Variable ----------------------- #

CurrentDIR = os.path.dirname(__file__)
ImageDIR = CurrentDIR + r"\Images"


# ----------------------- Functions ----------------------- #
#
# The "Exit to menu" button will destroy the current window and start the menu window.
# The "Send SQL Command!" button will send the Command to the Database without any feedback to the user!

def main():
    
    global root

    root = tk.Tk()
    root.title('Query Menu')
    root.geometry('420x50')

    tk.Label(root, text = 'SQL Command: ').grid(row = 0)

    global e

    e = Entry(
        root,
        width = 53,
        )

    SQLCommand = tk.Button(
        root,
        text = 'Send SQL Command!',
        command = lambda:[query()],
        borderwidth = 1
        )

    SQLCommand.grid(
        row = 2,
        column = 1
        )

    Exit = tk.Button(
        root,
        text = 'Exit to menu',
        command = lambda:[exit()],
        borderwidth = 1
    )

    Exit.grid(
        row = 2,
        padx = 1
    )

    e.grid(
        row = 0,
        column = 1
    )
        
    root.mainloop()

# Function to Establish Connection to the Database

def conn():
    global mydb
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        database="corona"
    )

# Function to send the desired command to the Database, recieve Feedback and give it to the user

def query():

    conn()

    sql = e.get()

    cursor = mydb.cursor()
    cursor.execute(sql)

# Function to return back to the Main menu.

def exit():

    root.destroy()

    Menu = str(r'python {}\Query_Menu.py').format(CurrentDIR)

    os.system(Menu)


# ----------------------- Main Method ----------------------- #

if __name__ == "__main__":
    main()