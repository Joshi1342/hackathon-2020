import mysql.connector, os
import tkinter as tk
from tkinter import *
from tkinter import Text


# ----------------------- Global Variables ----------------------- #

CurrentDIR = os.path.dirname(__file__)
ImageDIR = CurrentDIR + r"\Images"


# ----------------------- Functions ----------------------- #
#
# The "Exit to menu" button will destroy the current window and start the menu window.
# The "Send SQL!" button will send the query to the Database, recieves the feedback and displays it in a textbox.

def main():
    
    global root

    root = tk.Tk()
    root.title('Query Menu')
    root.geometry('420x100')

    tk.Label(root, text = 'SQL Query: ').grid(row = 0)

    global e

    e = Entry(
        root,
        width = 53
        )

    SaveSQL = tk.Button(
        root,
        text = 'Send SQL!',
        command = lambda:[query()],
        borderwidth = 1
        )

    SaveSQL.grid(
        row = 2,
        column = 1
        )

    Exit = tk.Button(
        root,
        text = 'Exit to menu',
        command = lambda:[exit()],
        borderwidth = 1
    )

    Exit.grid(
        row = 2,
        padx = 1
    )

    global SQLresult

    SQLresultlabel = tk.Label(
        root,
        text = 'SQL result: '
    )

    SQLresultlabel.grid(
        row = 1,
        column = 0
    )

    SQLresult = tk.Text(
        root,
        height = 3,
        width = 40
    )

    SQLresult.grid(
        row = 1,
        column = 1
    )

    e.grid(
        row = 0,
        column = 1
    )
        
    root.mainloop()

# Function to Establish Connection to the Database

def conn():

    global mydb
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        database="corona"
    )

# Function which sends the users query to the Database and displays the result

def query():

    conn()

    sql = e.get()

    cursor = mydb.cursor()
    cursor.execute(sql)

    global result

    result = cursor.fetchall()
    
    SQLresult.delete('1.0', END)
    SQLresult.insert(INSERT, result)

# Function to return back to the Main menu.

def exit():

    root.destroy()

    Menu = str(r'python {}\Query_Menu.py').format(CurrentDIR)

    os.system(Menu)

# ----------------------- Main Method ----------------------- #

if __name__ == "__main__":
    main()